---
layout: page
title: Teil 2.8 - Reverse Engineering
permalink: /docs/schulung_teil2_8/
---

Mit dem orcas-ant-Task [orcas_extract]({{site.baseurl}}/docs/ant-tasks/#orcas_extract) kann aus einem bestehenden Datenbankschema ein Abzug der Tabellen und Sequenzen in das passende orcas-Skriptformat erzeugt werden.

## Übung
Erweitere die build.xml um ein "extract"-Target, dass die Tabellenskripte für das Schema generiert.

Vergleiche die generierten Skripte mit den Original-Skripten.
