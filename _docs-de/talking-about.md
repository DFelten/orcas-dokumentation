---
layout: page
title: Orcas im Netz
permalink: /docs/talking-about/
---

Auf dieser Seite findet sich eine Sammlung von Artikeln und Dokumenten, in denen über Orcas berichtet wird.

## Blogartikel über Orcas

* [Ansätze für das Datenbankschema-Management oder wie geht Continuous Integration für die Datenbank](https://thecattlecrew.wordpress.com/2015/06/30/ansatze-fur-das-datenbankschema-management-oder-wie-geht-continuous-integration-fur-die-datenbank/) von Olaf Jessensky am 30. Juni 2015
* [Erste Schritte mit Orcas oder Continuous Delivery für die Datenbank leicht gemacht](https://thecattlecrew.wordpress.com/2015/08/04/erste-schritte-mit-orcas-oder-continuous-delivery-fuer-die-datenbank-leicht-gemacht/) von Olaf Jessensky am 4. August 2015

## Whitepaper über Orcas

* [Orcas: Continuous Delivery für die Datenbank](http://www.opitz-consulting.com/fileadmin/redaktion/veroeffentlichungen/whitepaper/whitepaper-orcas_sicher.pdf) von Olaf Jessensky
