---
layout: page
title: Teil 2.7 - Mehrere Targets
permalink: /docs/schulung_teil2_7/
---
## Übung

Die "build.xml" Datei soll so umgebaut werden, dass für die one_time_scripts, statics und replaceables jeweils einzelne Targets vorhanden sind (die dann über den ant-Aufruf auch separat gestartet werden können).

Das default-Target soll diese drei Einzeltargets in der richtigen Reihenfolge aufrufen.

