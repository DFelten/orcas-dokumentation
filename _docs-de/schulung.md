---
layout: page
title: Schulung
permalink: /docs/schulung/
---

**Diese Schulung befindet sich noch im Aufbau und ist momentan weder vollständig noch korrekt !**

## [Teil 1 - Basics]({{site.baseurl}}/docs/schulung_teil1_1)

## [Teil 2 - Orcas ant-Tasks]({{site.baseurl}}/docs/schulung_teil2_1)

## [Teil 3 - Orcas Extensions]({{site.baseurl}}/docs/schulung_teil3_1)

## Teil 4 (optional)

### dbdoc

### data-update

### sapp-client

### location-konzept

## [Teil 5 - Einsatzscenarien]({{site.baseurl}}/docs/schulung_teil5)

