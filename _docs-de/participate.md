---
layout: page
title: Wie kann ich mitmachen?
permalink: /docs/participate/
---

#Hilf uns bei der Weiterentwicklung

Generell ist Mitarbeit an Orcas willkommen! 

Wenn ein Änderungs-/Erweiterungs-Bedarf an Orcas besteht, sollte zunächst mal ein Issues auf unserer github-Seite dazu angelegt werden.
In diesem Issue sollte dann grob festgelegt werden was zu tun ist. Wenn jemand die Umsetzung übernehmen möchte, dann sollte das im <a href="{{ site.github_issues }}">Issue</a> vermerkt werden. Danach gilt die normale github-Vorgehensweise (fork, Änderung, pull-request).

Generell werden auch pull-requests ohne vorherige Abstimmung geprüft. Hierbei kann es aber dann leichter passieren, dass eine solche Änderung nochmal überarbeitet werden muss bzw. komplett abgelehnt wird.


