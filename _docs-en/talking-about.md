---
layout: page
title: Orcas in the Internet
permalink: /docs/talking-about/
---

On this page you will find a collection of articles and documents, which report on Orcas.

## German blog articles about Orcas

* [Approaches for the database schema management or how do Continuous Integration for the database](https://thecattlecrew.wordpress.com/2015/06/30/ansatze-fur-das-datenbankschema-management-oder-wie-geht-continuous-integration-fur-die-datenbank/) by Olaf Jessensky on the 30th June 2015
* [Getting Started with Orcas or easy Continuous Delivery for the database](https://thecattlecrew.wordpress.com/2015/08/04/erste-schritte-mit-orcas-oder-continuous-delivery-fuer-die-datenbank-leicht-gemacht/) by Olaf Jessensky on the 4th of August 2015

## Whitepaper about Orcas

* [Orcas: Continuous Delivery for the database](http://www.opitz-consulting.com/fileadmin/redaktion/veroeffentlichungen/whitepaper/whitepaper-orcas_sicher.pdf) by Olaf Jessensky
