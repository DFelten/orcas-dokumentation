---
layout: page
title: How to participate?
permalink: /docs/participate/
---

#Help us in further developing

In general, the collaboration on Orcas is welcome! 

If there is a need for change or extensions for Orcas, first an Issue should be created on our github page. In this Issue should then be roughly determined what to do. If someone wants to take over the implementation, then that should be noted in the <a href="{{ site.github_issues }}">Issue</a>. Thereafter, the normal GitHub procedure (fork, change, pull request) applies.

Generally also pull requests are checked without prior agreement. 

It may easily happen that such a change must be revised again or is completely rejected.
